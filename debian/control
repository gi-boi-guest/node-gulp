Source: node-gulp
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Section: javascript
Testsuite: autopkgtest-pkg-nodejs
Priority: optional
Build-Depends: debhelper (>= 11~),
               mocha,
               nodejs,
               pkg-js-tools (>= 0.9.8~),
               node-archy,
               node-chalk,
               node-deprecated,
               node-gulp-util,
               node-interpret,
               node-liftoff (>= 3.1~),
               node-marked-man,
               node-minimist,
               node-orchestrator,
               node-pretty-hrtime,
               node-q,
               node-rimraf,
               node-semver (>= 4.1.0),
               node-should,
               node-tildify,
               node-v8flags,
               node-vinyl-fs (>= 3.0.3-2~),
# for embedded expect
               node-function-bind,
               node-object-inspect,
# for embedded glob-watcher
               node-chokidar (>= 2.0~),
# for embedded undertaker
               node-es6-weak-map,
# for embedded gulp-cli
               node-concat-stream,
# for embedded yargs
               node-parse-json,
               node-pinkie-promise,
               node-normalize-package-data,
# for embbedded is-absolute and is-relative
               node-is-unc-path (>= 1.0~),
# for embedded object-defaults
               node-for-own
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/js-team/node-gulp
Vcs-Git: https://salsa.debian.org/js-team/node-gulp.git
Homepage: https://gulpjs.com

Package: gulp
Architecture: all
Depends: ${misc:Depends},
         nodejs,
         node-archy,
         node-chalk,
         node-defaults,
         node-deprecated,
         node-globule,
         node-gulp-util,
         node-interpret,
         node-liftoff (>= 3.1~),
         node-minimist,
         node-orchestrator,
         node-pretty-hrtime,
         node-semver (>= 4.1.0),
         node-tildify,
         node-v8flags,
         node-vinyl-fs (>= 3.0.3-2~),
# for embedded undertaker
         node-es6-weak-map,
# for embedded glob-watcher
         node-chokidar (>= 2.0~),
# for embedded gulp-cli
         node-concat-stream,
# for embedded yargs
         node-parse-json,
         node-pinkie-promise,
         node-normalize-package-data,
# for embbedded is-absolute and is-relative
         node-is-unc-path (>= 1.0~),
# for embedded object-defaults
         node-for-own
Description: streaming build system to automate painful or time-consuming tasks
 gulp is a toolkit that helps you automate painful or time-consuming tasks in
 your development workflow.
 .
 Platform-agnostic - Integrations are built into all major IDEs and people are
 using gulp with PHP, .NET, Node.js, Java, and other platforms.
 Strong Ecosystem - Use npm modules to do anything you want + over 2000 curated
 plugins for streaming file transformations
 Simple - By providing only a minimal API surface, gulp is easy to learn and
 simple to use
 .
 Node.js is an event-based server-side JavaScript engine.
